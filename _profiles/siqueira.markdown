---
layout: default
modal-id: Siqueira
title: Siqueira
date: 2018-10-18
img: siqueira1.png
alt: image-alt
site: https://siqueira.tech/
description: I am a Software Engineering and currently pursue Master Degree in
  Computer Science at the University of Sao Paulo. My research interests
  gravitate around OS, FLOSS, and Software Engineering. I am passionate about
  Operating Systems (OS) field, for me, OS is one of the human most significant
  achievements. As a result, I am dedicating many hours of my life trying to
  understand this area and contributing to GNU/Linux.
---
